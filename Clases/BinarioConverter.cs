﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaizonConverter.Clases
{
    class BinarioConverter : Converter
    {
        public BinarioConverter(int valor) : base(valor) { }

        public override string Convertir()
        {
            int n = Valor;
            string bin = "";

            while (n > 0)
            {
                bin = n % 2 + bin;
                n = n / 2;
                //n /= 2;
            }

            return bin;
        }
    }
}
