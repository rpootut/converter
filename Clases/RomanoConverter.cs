﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaizonConverter.Clases
{
    class RomanoConverter : Converter
    {
        private static int[] decimales = new int[] { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };
        private static string[] romanos = new string[] { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };

        public RomanoConverter(int valor) : base(valor) { }
        public override string Convertir()
        {
            string rom = "";
            int n = decimales.Length - 1;

            while (Valor > 0)
            {
                int valor = decimales[n];
                if (Valor >= valor)
                {
                    rom = rom + romanos[n];
                    this.valor -= valor;
                }
                else
                {
                    n = n - 1;
                    //n--;
                }
            }
            return rom;
        }
    }
}
