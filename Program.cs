﻿using PaizonConverter.Clases;
using System;

namespace PaizonConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Escribe el numero: ");
            int numero = int.Parse(Console.ReadLine());

            Converter binario = new BinarioConverter(numero);
            Converter romano = new RomanoConverter(numero);

            Console.WriteLine("Binario: " + binario);
            Console.WriteLine("Romano: " + romano);
        }
    }
}
