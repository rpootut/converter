﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaizonConverter.Clases
{
    abstract class Converter
    {
        protected int valor;
        public int Valor { get => valor; }

        public Converter(int valor)
        {
            this.valor = valor;
        }
        
        public abstract string Convertir();

        public override string ToString() => Convertir();
    }
}
